# enkele command om nodige dependencies te installeren:
#           pip install -r requirements.txt

import dns.resolver
import socket

wwwCheck = "www."

def domain():
    choosedomain = input("Typ het domein voor jouw DNS request: ")

    if wwwCheck in choosedomain:
        print('Typ het domein zonder "www." om crashes te voorkomen')
        domain()

    nameservers = dns.resolver.resolve(choosedomain,'NS')
    for server in nameservers:
        print("Nameserver: ", server.target)

    DNS_Cname = socket.gethostbyaddr(choosedomain)[0]
    DNS_HostIP = socket.gethostbyname(choosedomain)
    print("CNAME van opgegeven domein:", DNS_Cname)
    print("IP-Adres van opgegeven domein:", DNS_HostIP)


